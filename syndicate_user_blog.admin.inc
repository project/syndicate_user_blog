<?php

/**
 * @file
 * Code required only for the syndicate user blog admin settings form.
 */
function syndicate_user_blog_admin_settings(&$form_state) {
  $form['syn_user_blog'] = array(
    '#type' => 'fieldset',
    '#title' => t('Syndicate user blog Settings'),
  );
  $form['syn_user_blog']['syn_user_blog_feed_type'] = array(
    '#type' => 'radios',
    '#title' => t('Feed Node Types'),
    '#default_value' => variable_get('syn_user_blog_feed_type', 'feed'),
    '#options' => node_get_types('names'),
    '#description' => t('The node types where feed attached to')
  );
  $form['syn_user_blog']['syn_user_blog_node_type'] = array(
    '#type' => 'radios',
    '#title' => t('Node Types'),
    '#default_value' => variable_get('syn_user_blog_node_type', 'blog'),
    '#options' => node_get_types('names'),
    '#description' => t('The node types where the syndicated content will be stored')
  );
  return system_settings_form($form);
}
function syndicate_user_blog_admin_approval() {
  $form = array();
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => array('Approve', 'Delete'),
    '#default_value' => 'approve',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );
  $result = db_query("SELECT * FROM {syndicate_user_blog}");
  while ($row = db_fetch_object($result)) {
    $options[$row->syndicate_id] = '';
    $syndivated_user = user_load($row->syndicate_uid);
    $form['name'][$row->syndicate_id] = array('#value' => l($syndivated_user->name, "user/". $syndivated_user->uid));
    $form['title'][$row->syndicate_id]= array('#value' => $row->syndicate_title);
    $form['url'][$row->syndicate_id] = array('#value' => l($row->syndicate_url, $row->syndicate_url, array('absolute' => TRUE)));
    $form['status'][$row->syndicate_id] = array('#value' => ($row->syndicate_approval == 1 ? 'Approved' : 'Not Approved'));
  }
  $form['user_feed'] = array('#type' => 'checkboxes', '#options' => $options);
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  return $form;
}
function syndicate_user_blog_admin_approval_submit($form, &$form_state) {
  $user_feeds = $form_state['values']['user_feed'];
  switch ($form_state['values']['operation']) {
    case 0:
      foreach ($user_feeds as $key => $user_feed) {
        if ($user_feed) {
          $row = db_fetch_object(db_query("SELECT * FROM {syndicate_user_blog} WHERE syndicate_id = %d", $key));
          $node = new stdClass();
          $node->type = variable_get('syn_user_blog_feed_type', 'feed');
          if ($row->syndicate_title != '') {
            $node->title = t($row->syndicate_title);
          }
          else {
            $syndicated_user = user_load($row->syndicate_uid);
            $node->title = $syndicated_user->name ."\'s blog";
          }
          $node->feeds['FeedsHTTPFetcher']['source'] = $row->syndicate_url;
          $node->uid = $row->syndicate_uid;
          node_save($node);
          db_query("UPDATE {syndicate_user_blog}
                       SET syndicate_feed_nid = %d,
                           syndicate_approval = 1
                     WHERE syndicate_id = %d",
                           $node->nid,
                           $key
                  );
        }
      }
      break;
    case 1:
      foreach ($user_feeds as $key => $user_feed) {
        if ($user_feed) {
          $row = db_fetch_object(db_query("SELECT * FROM {syndicate_user_blog} WHERE syndicate_id = %d", $key));
          node_delete($row->syndicate_feed_nid);
          db_query("DELETE FROM {syndicate_user_blog} WHERE syndicate_id = %d", $key);
        }
      }
      break;
  }
 }